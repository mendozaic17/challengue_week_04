from validation import Validacion

class AgendaTelefonica(Validacion):
    
    contactos = [] 
    
    def __init__(self):
        self.contactos.append({"nombre": "Alan 01", "telefono": "90000001"})
        self.contactos.append({"nombre": "Alan 02", "telefono": "90000002"})
        self.contactos.append({"nombre": "Alan 03", "telefono": "90000003"})
        self.contactos.append({"nombre": "Alan 04", "telefono": "90000004"})
        self.contactos.append({"nombre": "Alan 05", "telefono": "90000005"})
        
        print('Metodo INIT de la clase AgendaTelefonica')
        
    def añadirContacto(self):
        pass
    
    def listarContacto(self):
        return self.contactos
   
    def buscarContacto(self, documento = '999999'):
        return self.contactos[1]
    
    def editarContacto(self):
        pass
    
    def eliminarContacto(self):
        pass

